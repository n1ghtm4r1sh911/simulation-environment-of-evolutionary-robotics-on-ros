
"use strict";

let Uint8ArrayTestMessage = require('./Uint8ArrayTestMessage.js');
let TestArray = require('./TestArray.js');
let Uint8Array3TestMessage = require('./Uint8Array3TestMessage.js');

module.exports = {
  Uint8ArrayTestMessage: Uint8ArrayTestMessage,
  TestArray: TestArray,
  Uint8Array3TestMessage: Uint8Array3TestMessage,
};
