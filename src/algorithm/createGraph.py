
import sys
import matplotlib.pyplot as plt
def graphics(gen, avg, value, fitness):
  gen = gen
  avg= avg
  if fitness=='max':
      maxi= value
      fig, ax1 = plt.subplots()
      #line1 = ax1.plot(gen, maxi, "b-", label="Maximum Fitness")
      line1 = ax1.plot(gen, maxi, "b-", label="Maximum Fitness")
      ax1.set_xlabel("Generation")
      ax1.set_ylabel("Maximum Fitness", color="b")
      for tl in ax1.get_yticklabels():
          tl.set_color("b")
      ax2 = ax1.twinx()
      line2 = ax2.plot(gen, avg, "r-", label="Average Fitness")
      ax2.set_ylabel("Average Fitness", color="r")
      #ax2.set_ylim(0,1.6)
      for tl in ax2.get_yticklabels():
          tl.set_color("r")
      lns = line1 + line2
      labs = [l.get_label() for l in lns]
      ax1.legend(lns, labs, loc="center right")
      plt.show()
  else:
      min= value
      fig, ax1 = plt.subplots()
      #line1 = ax1.plot(gen, maxi, "b-", label="Maximum Fitness")
      line1 = ax1.plot(gen, min, "b-", label="Minimum Fitness")
      ax1.set_xlabel("Generation")
      ax1.set_ylabel("Minimum Fitness", color="b")
      for tl in ax1.get_yticklabels():
          tl.set_color("b")
      ax2 = ax1.twinx()
      line2 = ax2.plot(gen, avg, "r-", label="Average Fitness")
      ax2.set_ylabel("Average Fitness", color="r")
      #ax2.set_ylim(0,8)
      for tl in ax2.get_yticklabels():
          tl.set_color("r")
      lns = line1 + line2
      labs = [l.get_label() for l in lns]
      ax1.legend(lns, labs, loc="upper right")
      plt.show()

def main():
    #python createGraph.py 0,1,2,3,4,5 7.15,3.05,2,2,2,2 2,2,2,2,2,2 min
    #python createGraph.py 0,1,2,3,4,5,6,7,8,9,10 0.221606,0.510682,1.11395,1.41132,1.44014,1.44657,1.45048,1.4519,1.44833,1.45082,1.45132       1.42543,1.44013,1.45044,1.45366,1.45557,1.45557,1.45557,1.45699,1.45699,1.4563,1.45623 max
    #python createGraph.py 0,1,2,3,4,5,6,7,8,9,10 254.909,252.464,250.11,249.61,240.22,235.378,237.567,244.862,238.063,245.3,247.725    245.9,10.5,10.5,10.5,10.5,11.5,11.5,12.3,12.3,12.5,12.5 min

    #python createGraph.py 0,1,2,3,4,5 0.26299,0.299887,0.340675,0.361914,0.367277,0.370352 0.357967,0.366873,0.374318,0.374451,0.375055,0.37872 max


    #python createGraph.py 0,1,2,3,4,5,6,7,8,9,10 247.525,235.695,216.822,170.851,155.768,220.012,240.991,239.168,234.214,244.085,248.919 132.5,12.6,12.4,9.1,9.1,9.1,9.1,9.1,9.1,13.3,13.3 min


    #[0, 1, 2, 3, 4, 5]
    gen= (sys.argv[1].split(','))
    #[8.3499999999999996, 5.9000000000000004, 4.1500000000000004, 3.7000000000000002, 3.1000000000000001, 3.0]
    avg= (sys.argv[2].split(','))
    #[1.0, 3.0, 3.0, 3.0, 3.0, 3.0]
    value= (sys.argv[3].split(','))
    #max or min
    fitness=sys.argv[4]
    graphics(gen, avg, value, fitness)

if __name__ == "__main__":
  	main()
