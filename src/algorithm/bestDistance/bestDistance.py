
from deap import base
from deap import creator
from deap import tools
from deap import algorithms
from gazebo_msgs.srv import GetModelState, SetModelState, GetJointProperties
from gazebo_msgs.msg import ModelState
from std_msgs.msg import Float64
from std_msgs.msg import Int16
import std_msgs
from geometry_msgs.msg import Pose
import random, math, rospy, sys, numpy
from std_srvs.srv import Empty
from operator import attrgetter
import matplotlib.pyplot as plt
import os
import yaml
from rosgraph_msgs.msg import Clock

creator.create ("FitnessMax", base.Fitness, weights=(1.0,))
creator.create ("Individual", list, fitness=creator.FitnessMax)
toolbox = base.Toolbox()


class bestDistance():

	def __init__(self, topics, time):
		#self.reset = rospy.ServiceProxy('gazebo/reset_simulation', Empty)
		self.controllers=[]
		self.time = time
		for count in range(len(topics)):
			con = rospy.Publisher(topics[count], Float64, queue_size=1)
			self.controllers.append(con)
		rospy.wait_for_service('/gazebo/get_model_state')
		rospy.wait_for_service('/gazebo/set_model_state')
		rospy.wait_for_service('/gazebo/unpause_physics')
		rospy.wait_for_service('/gazebo/pause_physics')
		self.pub = rospy.Publisher('/clock', Clock, queue_size=10)

		#self.model_coordinates = rospy.ServiceProxy('/gazebo/get_joint_properties', GetJointProperties)
		self.model_coordinates = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
		self.g_set_state = rospy.ServiceProxy("/gazebo/set_model_state",SetModelState)
		self.unpause = rospy.ServiceProxy('gazebo/unpause_physics', Empty)
		self.pause = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
		self.valor1 = 0
		self.valor2 = 0
		self.clockV=0
		self.timeCount=0
		#self.reset()
	def ramdonValue(self,individual):
		value1= random.randint(self.valor1, (self.valor1 + self.valor2))
		value2= random.randint(self.valor2, (self.valor1 + self.valor2))
		return individual([value1, value2])

	def posicion(self):
		object_coordinates = self.model_coordinates ("lmev","")
		y =  object_coordinates.pose.position.y
		x =  object_coordinates.pose.position.x
		return [x,y]

	def moveRobot(self, x, y):
		pose = Pose()
		pose.position.x = x
		pose.position.y = y
		pose.position.z = 0.070000
		pose.orientation.x = 0.0
		pose.orientation.y = 0.0
		pose.orientation.z = 0.0
		pose.orientation.w = 0.0
		state = ModelState()
		state.model_name = "lmev"
		state.pose = pose
		ret = self.g_set_state(state)

	def clockValue(self, value):
		self.clockV = value.clock.secs

	def evaluateVel(self,individual):
		#now = rospy.get_rostime()
		r= rospy.Rate(5)
		self.to = rospy.Subscriber('/clock', Clock,self.clockValue)
		rospy.wait_for_message('/clock', Clock)
		self.timeCount =  self.clockV + self.time
		#timeCount = now.secs+(float(self.time))
		while ((not rospy.is_shutdown()) and self.clockV <= self.timeCount):
			#for count in range(len(self.controllers)):
			self.controllers[0].publish(float(individual[0]))
			self.controllers[1].publish(float(individual[1]))
			r.sleep()
			#now= rospy.get_rostime()
		[x,y] = self.posicion()
 		#d=sqrt((x-0)2+(y-0)2) se pone 0 porque calculamos la distancia desde el (0,0)
		#distancia =math.sqrt(pow(x,2) + pow(y,2))
		self.moveRobot(0.0, 0.0)
		#self.reset()
		return y,




	def printJson(self, hol):
		act = open('bestInd', 'w')
          	#aqui esto es ya siempre a
		i=1
		for ind in hol:
			act.write(str(ind))
			act.write('\n')
               	#aqui
		act.close()

#https://github.com/cosmoharrigan/neuroevolution/blob/master/neuroevolution/train_cnn_ga.py
	def eaSimpleModified(self,population, toolbox, cxpb, mutpb, ngen, stats=None,
halloffame=None, verbose=__debug__):
		f = open('logfile', 'w')
		f.write('\n')
		logbook = tools.Logbook()
		logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])
		f.write("Initial values")
		f.write('\n')
		value=("Population: ", population)
		f.write (str(value)) #imprimimos la poblacion inicial
		f.write('\n')
    		# Evaluate the individuals with an invalid fitness
		invalid_ind = [ind for ind in population if not ind.fitness.valid]
		fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
		value= ("Fitness: ", fitnesses)
		f.write(str(value))
		f.write('\n')
		for ind, fit in zip(invalid_ind, fitnesses):
			ind.fitness.values = fit

		if halloffame is not None:
			halloffame.update(population) #en vez de insert antes ponia update
		record = stats.compile(population) if stats else {}
		logbook.record(gen=0, nevals=len(invalid_ind), **record)
		if verbose:
			print(logbook.stream)
		best = []
		best_ind = max(population, key=attrgetter("fitness"))
		best.append(best_ind)
		value=("Best: ", str(best_ind))
		f.write(str(value))
		f.write('\n')

    		# Begin the generational process
		for gen in range(1, ngen + 1):
			f.write('\n')
			value=(gen, " Generation")
			f.write (str(value))
			f.write('\n')
			# Select the next generation individuals
			offspring = toolbox.select(population, len(population))
			# Vary the pool of individuals
			offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)
			value=("Mutations: ", offspring)
			f.write(str(value)) #mutaciones
			f.write('\n')
			# Evaluate the individuals with an invalid fitness
			invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
			fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
			value =("Selection: ", invalid_ind)
			f.write(str(value)) #seleccion los nuevos
			f.write('\n')
			value=("Fitness: ", fitnesses)
			f.write(str(value)) #los entrena
			f.write('\n')
			for ind, fit in zip(invalid_ind, fitnesses):
				ind.fitness.values = fit
			# Save the best individual from the generation
			best_ind = max(offspring, key=attrgetter("fitness"))
			best.append(best_ind)
			# Update the hall of fame with the generated individuals
			if halloffame is not None:
				halloffame.update(offspring) #en vez de insert antes ponia update
			# Replace the current population by the offspring
			population[:] = offspring
			value =("Best: ", str(best_ind))
			f.write(str(value))
			f.write('\n')
			# Append the current generation statistics to the logbook
			record = stats.compile(population) if stats else {}
			logbook.record(gen=gen, nevals=len(invalid_ind), **record)
			if verbose:
				print(logbook.stream)
		f.write('\n')
		f.write(str(logbook))
		f.write('\n')
		f.write('\n')
		fit_max = logbook.select("max")
		value=("Best of all GEN: ", halloffame[0])
		f.write(str(value))
		f.write('\n')
		valor_fitness= max(fit_max)
		value=("with the fitness of: ", valor_fitness)
		f.write(str(value))
		f.write('\n')
		f.close()
		return population, logbook, best

	def graphics(self, logbook):
		gen = logbook.select("gen")
		avg= logbook.select("avg")
		maxi= logbook.select("max")
		fig, ax1 = plt.subplots()
		line1 = ax1.plot(gen, maxi, "b-", label="Maximum Fitness")
		ax1.set_xlabel("Generation")
		ax1.set_ylabel("Fitness", color="b")
		for tl in ax1.get_yticklabels():
			tl.set_color("b")
		ax2 = ax1.twinx()
		line2 = ax2.plot(gen, avg, "r-", label="Average Fitness")
		ax2.set_ylabel("Average", color="r")
		for tl in ax2.get_yticklabels():
			tl.set_color("r")
		lns = line1 + line2
		labs = [l.get_label() for l in lns]
		ax1.legend(lns, labs, loc="center right")
		plt.show()

	def algorithm(self, valor1, valor2, totalPoblacion):
		self.unpause()
		self.valor1 = valor1
		self.valor2 = valor2
		#self.Encoder_A_Subscriber = rospy.Subscriber('/Arm_Motor/target_cmd', Int16, self.Encoder_A_Processor)
		toolbox.register("individual", self.ramdonValue, creator.Individual)
		toolbox.register("population", tools.initRepeat,list, toolbox.individual)
		population = toolbox.population(totalPoblacion)

		hof = tools.HallOfFame(100)
		stats = tools.Statistics(lambda ind: ind.fitness.values)
		stats.register("avg", numpy.mean) #media
		stats.register("std", numpy.std) #error
		stats.register("min", numpy.min) #minimo
		stats.register("max", numpy.max) #maximo
		toolbox.register("evaluate", self.evaluateVel)
		toolbox.register("mate", tools.cxTwoPoint)
		toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=1.0, indpb=0.05)
		toolbox.register("select", tools.selTournament, tournsize=3)
		CXPB, MUTPB, NGEN = 0.6, 0.4, 5
		#population = algorithms.eaSimple(population, toolbox, cxpb=CXPB, mutpb=MUTPB, ngen=NGEN)
		#eaSimpleModified(population, toolbox, cxpb, mutpb, ngen, stats=None,
             		#halloffame=None, verbose=__debug__):
		pop, log, best = self.eaSimpleModified(population,
                                   toolbox,
                                   cxpb=CXPB,
                                   mutpb=MUTPB,
                                   ngen=NGEN,
                                   stats=stats,
                                   halloffame=hof,
				verbose=True)

		for control in self.controllers:
			control.publish(0)
		self.printJson (hof)
		self.graphics(log)
        	#print best
		#print pop
		#print log
		self.pause()
		return best

def main():
	rospy.init_node('EA_Algorithm', anonymous=True)
	valor1 =int(sys.argv[1])
	valor2= int(sys.argv[2])
	totalPoblacion = int(sys.argv[3])
	random.seed(None)
	topics = []
	time= []
	controllers = yaml.load(open("controller.yaml", 'r'), Loader=yaml.CLoader)
	time = int(controllers['Time']['SimulationTime'])
	for controller in controllers['Code']['Controllers']:
		topics.append(controller.get('ros_name_topic'))
		#velocity[controller['ros_name_prefix']] = controller.get('ros_name_prefix')

	al = bestDistance(topics, time)
	al.algorithm(valor1, valor2, totalPoblacion)

if __name__ == "__main__":
	main()
