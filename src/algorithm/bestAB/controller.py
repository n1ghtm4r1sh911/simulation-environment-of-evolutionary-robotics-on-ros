import rospy
import yaml
from std_msgs.msg import Float64, Float32
from std_srvs.srv import Empty
from rospy.exceptions import ROSException
from rosgraph_msgs.msg import Clock
from rospy_message_converter import message_converter
from gazebo_msgs.srv import GetModelState, SetModelState, GetJointProperties
from gazebo_msgs.msg import ModelState
from geometry_msgs.msg import Pose
import time
class playInd():
	def __init__(self):
		#self.controllers = [[],[],[],[]]
		self.control=[]
		self.touch=[]
		self.velocity = []
		self.time = 0
		self.sensors = [[],[],[]]
		self.sensor = []
		self.clockV=0
		self.timeCount=0
		self.pub = rospy.Publisher('/clock', Clock, queue_size=10)
		try:
			rospy.wait_for_service('/gazebo/unpause_physics', 5)
			rospy.wait_for_service('/gazebo/set_model_state', 5)
			rospy.wait_for_service('/gazebo/pause_physics', 5)
			self.unpause = rospy.ServiceProxy('gazebo/unpause_physics', Empty)
			self.pause = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
			self.g_set_state = rospy.ServiceProxy("/gazebo/set_model_state",SetModelState)
			self.moveRobot(0.0, 0.0)
		except ROSException:
			self.unpause = Empty
			self.pause= Empty


	def moveRobot(self, x, y):
		pose = Pose()
		pose.position.x = x
		pose.position.y = y
		pose.position.z = 0.070000
		pose.orientation.x = 0.0
		pose.orientation.y = 0.0
		pose.orientation.z = 0.0
		pose.orientation.w = 0.0
		state = ModelState()
		state.model_name = "lmev"
		state.pose = pose
		ret = self.g_set_state(state)

	def parse(self):
		controllers = yaml.load(open("controller.yaml", 'r'), Loader=yaml.CLoader)
		self.time = int(controllers['Time']['SimulationTime'])
		self.controlA = [[],[],[],[]]
		self.ver= controllers['Mode']['SimulationMode']

		for controller in controllers['Code']['Controllers']:
			self.controlA[0].append(controller.get('id'))
			self.controlA[1].append(controller.get('ros_name_topic'))
			t = message_converter.convert_dictionary_to_ros_message (controller.get('tipo'),{ 'data': controller.get('tipo') })
			self.controlA[2].append(t)
			self.controlA[3].append(controller.get('data'))

		for count in range(len(self.controlA[0])):
			dataType = type(self.controlA[2][count])
			con = rospy.Publisher(self.controlA[1][count], dataType, queue_size=1)
			self.control.append(con)
		#velocity[controller['ros_name_prefix']] = controller.get('ros_name_prefix')

		if self.ver=='virtual':
			for sens in controllers['Code']['Sensors']:
				self.sensors[0].append(sens.get('id'))
				self.sensors[1].append(sens.get('ros_name_topic'))
				t = message_converter.convert_dictionary_to_ros_message (sens.get('tipo'),{ 'data': sens.get('tipo') })
				self.sensors[2].append(t)
		else:
			for sens in controllers['Code']['Sensors']:
				self.sensors[0].append(sens.get('id'))
				self.sensors[1].append(sens.get('ros_name_topic'))
			value=controllers['Code']['Sensors']
			t = message_converter.convert_dictionary_to_ros_message (value[0].get('tipo'),{ 'range': value[0].get('tipo') })
			self.sensors[2].append(t)
			t1 = message_converter.convert_dictionary_to_ros_message (value[1].get('tipo'),{ 'data': value[1].get('tipo') })
			self.sensors[2].append(t1)

#	def ultrasonicValue(self, value, type ):
#		self.setUltraValue(value.data)

#	def setUltraValue(self, value):
#		self.sensor.append(value)

#	def getUltraValue(self):
#		value = float(self.sensor.pop())
#		return value

	def sensorValue(self, value, tipe):
		if tipe == 'ultrasonic':
			if self.ver == 'virtual':
				self.setUltraValue(value.data)
			else:
				self.setUltraValue((value.range)*100)
		else:
			if tipe == 'touch':
				self.touch.append(value.data)

	def clockValue(self, value):
		self.clockV = value.clock.secs

	def setUltraValue(self, value):
		self.sensor.append(value)

	def getUltraValue(self):
		value = float(self.sensor.pop())
		return value

	def getTouchValue(self):
		value = float(self.touch.pop())
		return value

	def run(self):
		self.unpause()
		#rospy.Subscriber('/clock', Clock, self.clockValue)
		#rospy.wait_for_message('/clock', Clock)
		r= rospy.Rate(100)
		#t = rospy.Time(rospy.Time.now().secs + self.time)
		self.to = rospy.Subscriber('/clock', Clock,self.clockValue)
		rospy.wait_for_message('/clock', Clock)
		self.timeCount =  self.clockV + self.time
		while ((not rospy.is_shutdown()) and  self.clockV<=self.timeCount ):
			#now = rospy.get_rostime()
			#msg = Clock()
			#msg.clock = t
			#self.pub.publish(msg)
			#for count in range(len(self.controllers)):
			for countS in range(len(self.sensors[0])):
				dataType = type(self.sensors[2][countS])
				self.ult = rospy.Subscriber(self.sensors[1][countS], dataType, self.sensorValue, self.sensors[0][countS])
				rospy.wait_for_message(self.sensors[1][countS], dataType)
			lastInput = self.getUltraValue()
			#tou = self.getTouchValue()
			if lastInput<8:
				#print lastInput
				break
			potencia = (self.controlA[3][0]*lastInput + self.controlA[3][1])
			#print potencia
			#print lastInput
			for count in range(len(self.controlA[0])):
				self.control[0].publish(float(potencia))
				self.control[1].publish(float(potencia))
			r.sleep()
			#now= rospy.get_rostime()
			#if now.secs >=timeCount:
			#	break
		maxIt=100
		it=0
		while not rospy.is_shutdown():
		#	for count in range(len(self.controllers)):
                #        	self.controllers[count].publish(0)
			for count in range(len(self.controlA[0])):
				self.control[count].publish(float(0))
			it=it+1
			if (maxIt >= it):
				break
		self.pause()

def main():
	rospy.init_node('Eval_node', anonymous=True, disable_rostime=True)
	play = playInd()
	play.parse()
	play.run()

if __name__ == "__main__":
	main()
