
from deap import base
from deap import creator
from deap import tools
from std_msgs.msg import Int16
import random, math



#------------Creator------------#
#Creamos individuos personalizados

#definimos la clase fitnessMax. Hereda la clase Fitness del modulo deap.base que contiene un atributo llamado weights
#El cual solo tiene un valor en la tupla para indicar que estamos maximizando unicamente

creator.create ("FitnessMax", base.Fitness, weights=(1.0,))

#Creamos la clase Individual, que hereda list y que contiene nuestra clase FitnessMax

creator.create ("Individual", list, fitness=creator.FitnessMax)

#------------toolbox------------#
#Usamos nuestras clases personalizadas para crear tipos que representen a nuestros individuos,
#asi como a toda nuestra poblacion. metemos los datos

toolbox = base.Toolbox()

#Creamos 2 valores aleatorios y se los ponemos a los motores
#def positionValue():
#	value1= random.randint(0,50)
#	value2= random.randint(0,50)
#	#setLeftMotorValue(value1)
#	#setRightMotorValue(value2)
#	return [value1, value2]

IND_SIZE=2 #2 motores, 2 valores aleatorios
toolbox = base.Toolbox()
#registramos lo que queremos que nos devuelva, queremos que sea valor aleatorio
toolbox.register("attr_int", random.randint,0,100)

#indicamos el numero de veces que se debe de repetir
toolbox.register("individual", tools.initRepeat, creator.Individual,
                 toolbox.attr_int, n=IND_SIZE)



####----------Obtenemos una muestra-------####

ind1 = toolbox.individual()
#print "MUESTRA"
#print ind1 #esto me devuelve una tupla aleatoria
#print ind1.fitness.valid #me dice si ha sido entrenado

#####----------EVALUACION--------####

#simplemente me suma ambos valores
def evaluate(individual):
	a = sum(individual)
	#print a
	return a,

ind1.fitness.values = evaluate(ind1)
print "EVALUACION"
print ind1
print ind1.fitness.valid
print ind1.fitness
#si le pones fitness al final te devuelve el resultado pasado  por la funcion fitness...sino te devuelve la tupla

####--------MUTACION---------####
##se cambian ligeramente los valores
#deap.tools.mutUniformInt(individual, low, up, indpb)
#toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=1.0, indpb=0.05)
mutant = toolbox.clone(ind1)
ind2, = tools.mutGaussian(mutant, 0, 7, 1)

#print "MUTACION"
#print ind1
#print ind2
#print ind1 is mutant
#print ind2 is mutant    # True
#print ind2 is ind1    # False

####------CROSSOVER---------####
##Se realizan cruces entre individuos, esta vez ambos tienen mismo valor
child1, child2 = [toolbox.clone(ind) for ind in (ind1, ind2)]
tools.cxBlend(child1, child2, 0.2)

#print "CROSSOVER"
#print ind1
#print ind2
#print child1
#print child2
###------SELECCION--------####
#deap.tools.selBest(individuals, k, fit_attr='fitness')
child1.fitness.values = evaluate(child1)
child2.fitness.values = evaluate(child2)
selected = tools.selBest([child1, child2], 1)
#print "SELECTION"
#print child1
#print child1.fitness
#print child2
#print child2.fitness
#print child1 in selected
#print child2 in selected
#print selected



###TE DEVUELVE EL QUE MAS DE ACERQUE A LO QUE TE DE FITNESS
##EN CASO DE CHILD1=[54.03523007117349, 10.0] = 64.03523007117
# Y CHILD2=[53.87832744337071, 10.0] = 63.87832744337
#FITNESS = (64.0,)
# 64.03523007117-64 =0.03523007117
#y 64-63.87832744337=0.12167255663
#Escoge el CHILD1 porque se acercca mas al resultado
