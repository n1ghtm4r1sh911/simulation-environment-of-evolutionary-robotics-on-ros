#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64


class Conversion:
    def __init__(self):
        self.right = rospy.Publisher('/Left_Wheel_Motor/vel_cmd', Float64, queue_size=10)
        self.left = rospy.Publisher('/Right_Wheel_Motor/vel_cmd', Float64, queue_size=10)
        self.motor_data = [0,0]
        self.Rate = rospy.Rate(2)
    def conv(self, data):
        #100 --> 34
        #data --> x
        if data>=100:
            return 18.33
        return (data*18.33)/100

    def motorValue(self, value, motor):
        if motor == 1:
            self.motor_data[0] = value.data
        else:
            self.motor_data[1] = value.data

    def send(self):
        self.l= rospy.Subscriber('Conversion/left_motor', Float64, self.motorValue, 1)
        self.r= rospy.Subscriber('Conversion/right_motor', Float64, self.motorValue, 2)
        while not rospy.is_shutdown():
            left_value = self.conv (self.motor_data[0])
            self.left.publish(left_value*(-1))
            right_value = self.conv (self.motor_data[1])
            self.right.publish(right_value*(-1))
            self.Rate.sleep()
def main():
    rospy.init_node('Conversion_node', anonymous=False)
    cn = Conversion()
    cn.send()
    try:
        # Listens for new msgs in the subscribed topics
        rospy.spin()
    except KeyboardInterrupt:
		print "Shutting down Conversion"

if __name__ == "__main__":
	main()
