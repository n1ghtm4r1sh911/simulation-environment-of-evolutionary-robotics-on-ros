# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/miguel/TFG/real/src/ros_controllers/joint_trajectory_controller/include".split(';') if "/home/miguel/TFG/real/src/ros_controllers/joint_trajectory_controller/include" != "" else []
PROJECT_CATKIN_DEPENDS = "actionlib;angles;roscpp;urdf;control_toolbox;controller_interface;hardware_interface;realtime_tools;control_msgs;trajectory_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-ljoint_trajectory_controller".split(';') if "-ljoint_trajectory_controller" != "" else []
PROJECT_NAME = "joint_trajectory_controller"
PROJECT_SPACE_DIR = "/home/miguel/TFG/real/devel"
PROJECT_VERSION = "0.13.3"
