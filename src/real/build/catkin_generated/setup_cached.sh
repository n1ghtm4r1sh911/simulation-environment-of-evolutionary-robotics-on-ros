#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/miguel/TFG/real/devel:/opt/ros/kinetic"
export LD_LIBRARY_PATH="/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu"
export PKG_CONFIG_PATH="/opt/ros/kinetic/lib/pkgconfig:/opt/ros/kinetic/lib/x86_64-linux-gnu/pkgconfig"
export PWD="/home/miguel/TFG/real/build"
export PYTHONPATH="/opt/ros/kinetic/lib/python2.7/dist-packages"
export ROSLISP_PACKAGE_DIRECTORIES="/home/miguel/TFG/real/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/miguel/TFG/real/src:/opt/ros/kinetic/share"