import rospy
import os
from std_msgs.msg import Float64
from std_msgs.msg import String
from sensor_msgs.msg import Imu
from sensor_msgs.msg import Range
from std_msgs.msg import Bool
from std_msgs.msg import ColorRGBA


def getTime (rotation, velocity):
        #V=k/h (ti*dis)
        #http://cefire.edu.gva.es/pluginfile.php/175176/mod_resource/content/0/CONTENIDO2/EQUIVALENCIAS_FISICAS.pdf
        distanceR = 59.5
        timeR = 4.59067606926
        realVA = abs(float(distanceR))/(float(timeR))
        realV = abs((float(velocity)) * float(realVA))/5
        diameter=17.9 #17,9 cm ----> 1 rotation
        distance= float(diameter)*float(rotation)
        tim=(float(distance))/(float(realV)) #T=D/V
        return float(tim)

def callback (data, valor):
        if valor=='giro':
                rospy.loginfo("Orientation x:  %f",data.orientation.x)
                rospy.loginfo("Orientation y:  %f",data.orientation.y)
                rospy.loginfo("Orientation z:  %f",data.orientation.z)
        elif valor == 'ultra':
                rospy.loginfo("Field of view: %f", data.field_of_view)
                rospy.loginfo("Min range: %f", data.min_range)
                rospy.loginfo("Max range: %f", data.max_range)
                rospy.loginfo("Next object: %f", data.range)
        elif valor == 'touch':
                rospy.loginfo(data.data)
        elif valor == 'color':
                rospy.loginfo("R: %f", data.r)
                rospy.loginfo("G: %f", data.g)
                rospy.loginfo("B: %f", data.b)
                rospy.loginfo("A: %f", data.a)

def motors(port, vel, tim, valor):
        if (int(valor)==1):
                pub= rospy.Publisher ('/ev3dev/OutPort' + port[0] + '/command', Float64, queue_size=10)
                rospy.init_node('talker', anonymous=True)
                r = rospy.Rate(100) #10 hz ----> 0,1 seg
                r1 = rospy.Rate(1000)
                maxIt=100
                it=0
                t=0
                if (float(tim) != 0): #If we are using time, first we move motors and then we stop them
                        while (not rospy.is_shutdown()) and (t<=float(tim)):
				pub.publish(float(vel[0]))
                                t += 0.001
                                r1.sleep()
                        while (not rospy.is_shutdown()) and (it<=maxIt):
                                pub.publish(float(0.0))
                                it += 1
                                r.sleep()
                else:
                        while (not rospy.is_shutdown()) and (it<=maxIt):
                                pub.publish(float(vel[0]))
                                it += 1
                                r.sleep()


        if (int(valor)==2):
                pub= rospy.Publisher ('/ev3dev/OutPort' + port[0] + '/command', Float64, queue_size=10)
                pubB= rospy.Publisher ('/ev3dev/OutPort' + port[1] + '/command', Float64, queue_size=10)
                rospy.init_node('talker', anonymous=True)
                r = rospy.Rate(10)
                r1= rospy.Rate(10)
                maxIt=100
                it=0
                t=0

                if (float(tim) != 0): #If we are using time, first we move motors and then we stop them
			r.sleep() #To start the motors at same time
	       		while (not rospy.is_shutdown()) and (t<=float(tim)):
                        	pub.publish(float(vel[0]))
                                pubB.publish(float(vel[1]))
                                t += 0.1
                                r1.sleep()
                        while (not rospy.is_shutdown()) and (it<=maxIt):
                                pub.publish(float(0.0))
                                pubB.publish(float(0.0))
                                it += 1
                                r.sleep()
                else:
                        while (not rospy.is_shutdown()) and (it<=maxIt):
                                pub.publish(float(vel[0]))
                                pubB.publish(float(vel[1]))
                                it += 1
                                r.sleep()


        if (int(valor)==3):
                pub= rospy.Publisher ('/ev3dev/OutPort' + port[0] + '/command', Float64, queue_size=10)
                pubB= rospy.Publisher ('/ev3dev/OutPort' + port[1] + '/command', Float64, queue_size=10)
                pubC= rospy.Publisher ('/ev3dev/OutPort' + port[2] + '/command', Float64, queue_size=10)
                rospy.init_node('talker', anonymous=True)
                r = rospy.Rate(100)
                r1 = rospy.Rate(10)
                maxIt=100
                it=0

                if (float(tim) != 0): #If we are using time, first we move motors and then we stop them
                        while (not rospy.is_shutdown()) and (t<=float(tim)):
                                pub.publish(float(vel[0]))
                                pubB.publish(float(vel[1]))
                                pubC.publish(float(vel[2]))
                                t += 0.1
                                r1.sleep()
                        while (not rospy.is_shutdown()) and (it<=maxIt):
                                pub.publish(float(0.0))
                                pubB.publish(float(0.0))
                                pubC.publish(float(0.0))
                                it += 1
                               	r.sleep()
                else:
                        while (not rospy.is_shutdown()) and (it<=maxIt):
                                pub.publish(float(vel[0]))
                                pubB.publish(float(vel[1]))
                                pubC.publish(float(vel[2]))
                                it += 1
                                r.sleep()

def loop():
        os.system('roslaunch h4r_ev3_launch load.launch ev3_hostname:=ev3dev &')
        os.system('PID=$!')
        valor=os.system('echo $PID')
        while True:
                line = raw_input (">>")
                try:
                        if len(line)>1:
                                line_array = line.split()
                                print line_array
                                if line_array[0] == 'exit':
                                        os.system('kill -TERM ' + str(valor))
                                        break
                                elif line_array[0] == 'stop':
                                        if line_array[1] == 'A':
                                                pub = rospy.Publisher ('/ev3dev/OutPortA/command', Float64, queue_size=10)
                                                rospy.init_node('talker', anonymous=True)
                                                r = rospy.Rate(100)
                                                it = 0
                                                maxIt=100
                                                while (not rospy.is_shutdown()) and (it<=maxIt):
                                                        pub.publish(float(0.0))
                                                        it += 1
                                                        r.sleep()
                                        elif line_array[1] == 'B':
                                                pub = rospy.Publisher ('/ev3dev/OutPortB/command', Float64, queue_size=10)
                                                rospy.init_node('talker', anonymous=True)
                                                r = rospy.Rate(100)
                                                it = 0
                                                maxIt=100
                                                while (not rospy.is_shutdown()) and (it<=maxIt):

                                                        pub.publish(float(0.0))
                                                        it += 1
                                                        r.sleep()
                                        elif line_array[1] == 'C':
                                                pub = rospy.Publisher ('/ev3dev/OutPortC/command', Float64, queue_size=10)
                                                rospy.init_node('talker', anonymous=True)
                                                r = rospy.Rate(100)
                                                it = 0
                                                maxIt=100
                                                while (not rospy.is_shutdown()) and (it<=maxIt):
                                                        pub.publish(float(0.0))
                                                        it += 1
                                                        r.sleep()
                                        elif line_array[1] == 'all':
                                                pub = rospy.Publisher ('/ev3dev/OutPortA/command', Float64, queue_size=10)
                                                pubB = rospy.Publisher ('/ev3dev/OutPortB/command', Float64, queue_size=10)
                                                pubC = rospy.Publisher ('/ev3dev/OutPortC/command', Float64, queue_size=10)
                                                rospy.init_node('talker', anonymous=True)
                                                r = rospy.Rate(100)
                                                it = 0
                                                maxIt=100
                                                while (not rospy.is_shutdown()) and (it<=maxIt):
                                                        pub.publish(float(0.0))
                                                        pubB.publish(float(0.0))
                                                        pubC.publish(float(0.0))
                                                        it += 1
                                                        r.sleep()
                                elif line_array[0] =='sensor':
                                        if line_array[1] == 'giro':
                                                rospy.init_node('listener', anonymous=True)
                                                sus=rospy.Subscriber('/Gyroscope_Sensor', Imu, callback, 'giro')
                                                it = 0
                                                maxIt=20
                                                r = rospy.Rate(100)
                                                while (it<=maxIt):
                                                        it+=1
                                                        r.sleep()
                                                sus.unregister()
                                        elif line_array[1] == 'ultra':
                                                rospy.init_node('listener', anonymous=True)

                                                sus=rospy.Subscriber('/Ultrasonic_Sensor', Range, callback, 'ultra')
                                                it = 0
                                                maxIt=20
                                                r = rospy.Rate(100)
                                                while (it<=maxIt):
                                                        it+=1
                                                        r.sleep()
                                                sus.unregister()

                                        elif line_array[1] == 'light':
                                                rospy.init_node('listener', anonymous=True)
                                                sus=rospy.Subscriber('/Color_Sensor/Ambient', ColorRGBA, callback, 'color')
                                                it = 0
                                                maxIt=20
                                                r = rospy.Rate(100)
                                                while (it<=maxIt):
                                                        it+=1
                                                        r.sleep()
                                                sus.unregister()
                                        elif line_array[1] == 'touch':
                                                rospy.init_node('listener', anonymous=True)
                                                sus=rospy.Subscriber('/Touch_Sensor', Bool, callback, 'touch')
                                                it = 0
                                                maxIt=20
                                                r = rospy.Rate(100)
                                                while (it<=maxIt):
                                                        it+=1
                                                        r.sleep()
                                                sus.unregister()

                                elif line_array[0] == 'motor':
                                        portArray = [None]*1
                                        velArray = [0]*1
                                        portArray[0]= line_array[1]
                                        velArray[0]= line_array[2]
                                        rota = raw_input('Insert number of rotation:  ')
                                        rotArray = rota.split()
                                        rot=rotArray[0]
                                        tim = getTime (rot, line_array[2])
                                        motors(portArray,velArray, tim,1)

                                elif line_array[0] =='motors':
                                        num=line_array[1]
                                        portArray = [None]*(int(num))
                                        velArray = [0]*(int(num))
                                        for count in range(int(num)):
                                                motor = raw_input ('Insert ' + str(count+1) + ' motor: ')
                                                motorarray = motor.split()
                                                portArray[count] = motorarray[1]
                                                velArray[count] = motorarray[2]
                                        rota = raw_input('Insert number of rotation: ')
                                        rotArray = rota.split()
                                        rot=rotArray[0]
                                        tim = getTime (rot, motorarray[2])
                                        motors(portArray,velArray, tim, num)

                                elif line_array[0] == 'help':
                                        print ('options:' + '\n' +
                                        'write "exit" if you want to leave the program' + '\n' +
                                        'write "stop [Port:char]" if you want to stop an specific motor' + '\n' +

                                       '                       you can use "all" if you want to stop all the motors' + '\n' +
                                        'write "motor [Port:char] [velocity:float]" if you want to manage the motors' + '\n' +
                                        '               then the program will ask you about the rotation: ' + '\n' +
                                        '                       if you answer 0 ---> is like run-forever ' + '\n' +
                                        '                       if you answer [N:int] ---> the motors will ride N rotations ' + '\n' +
                                        'write "sensor [Type:string]" if you want to manage the sensors' + '\n' +
                                        '                       type could be: giro, ultra, light or touch' + '\n' +
                                        'write "motors [N:int]" if you want to take control in several motors simultaneously. It is usefull to full to ride 2 wheels at the same time  ' + '\n' +
                                        '               you should do several tests, use the same velocity to calculate the rotations ' + '\n')

                                else:
                                        print ('I dont know what do you mean with ' + line_array[0] + ' please write "help" to watch the options')

                except IndexError:
                        print('You have not written the required values, please enter "help" to watch the options')

                except ValueError:
                        print('You have written some value in a wrong way, please enter "help" to watch the options')

if __name__ == '__main__':
        try:
                loop()
        except rospy.ROSInterruptException:
                pass
