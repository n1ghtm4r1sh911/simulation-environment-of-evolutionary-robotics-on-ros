 #!/bin/bash

# Cleans workspace
killall rosmaster
killall gzserver
killall gzclient
sleep 5
# Source the setup.bash
. ~/TFG/lmev/devel/setup.bash
# Run the necessary commands for the correct initialization
# You can add your own commands here
# --tab -e "yourcommand"
# paused=true doesnt work so i use rospy to start the simulation
#if u want to use rospy.sleep you must set time to false because if u put time:=true rospy.sleep will just block infinitely

# To load gazebo with GUI
#gnome-terminal --tab -e "roslaunch lmev_gazebo lmev.launch debug:=false paused:=true gui:=true time:=true rec:=false " --tab -e "rosrun image_view image_view image:=/lent/image_raw" --tab -e "rosrun lmev_gazebo Main_Controller.py" --tab -e "rosrun lmev_gazebo conversion.py"

# To load gazebo without GUI
gnome-terminal --tab -e "roslaunch lmev_gazebo lmev.launch debug:=false paused:=true gui:=false time:=true rec:=false " --tab -e "rosrun image_view image_view image:=/lent/image_raw" --tab -e "rosrun lmev_gazebo Main_Controller.py" --tab -e "rosrun lmev_gazebo conversion.py"

# To load real robot.
#gnome-terminal --tab -e "roslaunch h4r_ev3_launch load.launch ev3_hostname:=ev3dev" --tab -e "rosrun gazebo_ros gzserver" --tab -e "rosrun h4r_ev3_launch conversion.py"
