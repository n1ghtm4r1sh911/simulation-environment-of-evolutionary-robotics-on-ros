set(_CATKIN_CURRENT_PACKAGE "lmev_gazebo")
set(lmev_gazebo_VERSION "0.0.0")
set(lmev_gazebo_MAINTAINER "Steven Palma Morera <imstevenpm.work@gmail.com>")
set(lmev_gazebo_PACKAGE_FORMAT "1")
set(lmev_gazebo_BUILD_DEPENDS "gazebo_plugins" "gazebo_ros" "roscpp")
set(lmev_gazebo_BUILD_EXPORT_DEPENDS "gazebo_plugins" "gazebo_ros" "roscpp")
set(lmev_gazebo_BUILDTOOL_DEPENDS "catkin")
set(lmev_gazebo_BUILDTOOL_EXPORT_DEPENDS )
set(lmev_gazebo_EXEC_DEPENDS "gazebo_plugins" "gazebo_ros" "roscpp")
set(lmev_gazebo_RUN_DEPENDS "gazebo_plugins" "gazebo_ros" "roscpp")
set(lmev_gazebo_TEST_DEPENDS )
set(lmev_gazebo_DOC_DEPENDS )
set(lmev_gazebo_URL_WEBSITE "")
set(lmev_gazebo_URL_BUGTRACKER "")
set(lmev_gazebo_URL_REPOSITORY "")
set(lmev_gazebo_DEPRECATED "")