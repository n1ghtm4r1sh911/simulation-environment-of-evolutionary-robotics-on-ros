#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/miguel/TFG/lmev/src/rospy_message_converter/rospy_message_converter"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/miguel/TFG/lmev/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/miguel/TFG/lmev/install/lib/python2.7/dist-packages:/home/miguel/TFG/lmev/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/miguel/TFG/lmev/build" \
    "/usr/bin/python" \
    "/home/miguel/TFG/lmev/src/rospy_message_converter/rospy_message_converter/setup.py" \
    build --build-base "/home/miguel/TFG/lmev/build/rospy_message_converter/rospy_message_converter" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/miguel/TFG/lmev/install" --install-scripts="/home/miguel/TFG/lmev/install/bin"
