# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/miguel/TFG/lmev/src/real/src/ros_controllers/imu_sensor_controller/include".split(';') if "/home/miguel/TFG/lmev/src/real/src/ros_controllers/imu_sensor_controller/include" != "" else []
PROJECT_CATKIN_DEPENDS = "controller_interface;hardware_interface;pluginlib;realtime_tools;roscpp;sensor_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-limu_sensor_controller".split(';') if "-limu_sensor_controller" != "" else []
PROJECT_NAME = "imu_sensor_controller"
PROJECT_SPACE_DIR = "/home/miguel/TFG/lmev/devel"
PROJECT_VERSION = "0.13.3"
